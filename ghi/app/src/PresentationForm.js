import { useEffect, useState } from 'react';

function PresentationForm() {

    const [presenterName, setPresenterName] = useState("");
    const [email, setEmail] = useState("");
    const [companyName, setCompanyName] = useState("");
    const [titleName, setTitleName] = useState("");
    const [synopsis, setSynopsis] = useState("");
    const [conferences, setConferences] = useState([]);
    const [conference, setConference] = useState("");

    const handleNameChange = e => {
        const value = e.target.value;
        setPresenterName(value);
    }
    const handleEmailChange = e => {
        const value = e.target.value;
        setEmail(value);
    }
    const handleCompanyChange = e => {
        const value = e.target.value;
        setCompanyName(value);
    }
    const handleTitleChange = e => {
        const value = e.target.value;
        setTitleName(value);
    }
    const handleSynopsisChange = e => {
        const value = e.target.value;
        setSynopsis(value);
    }
    const handleConferenceChange = e => {
        const value = e.target.value;
        setConference(value);
    }

    const fetchData = async () => {

        const url = 'http://localhost:8000/api/conferences/';

        const response = await fetch(url);

        if (response.ok) {
          const data = await response.json();
          setConferences(data.conferences);
          }
        }

      useEffect(() => {
        fetchData();
      }, []);

    const handleSubmit = async (e) => {
        e.preventDefault();

        const data = {};

        data.presenter_name = presenterName;
        data.presenter_email = email;
        data.company_name = companyName;
        data.title = titleName;
        data.synopsis = synopsis;
        data.conference = conference; //conference variable is just an href


        const conferenceId = conference.split('/').reverse()[1]; //this gets the id from the href
        const url = `http://localhost:8000/api/conferences/${conferenceId}/presentations/`;
        const json = JSON.stringify(data);

        const fetchConfig = {
            method: "POST",
            body: json,
            headers: {
            'Content-Type': 'application/json',
            }
        }

        const response = await fetch(url, fetchConfig);

        if (response.ok) {
            const newPresentation = await response.json();
            console.log(newPresentation);

            setPresenterName('');
            setEmail('');
            setCompanyName('');
            setTitleName('');
            setSynopsis('');
            setConference('');
        }
        }

    return(
    <div className="container">
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new presentation</h1>
          <form onSubmit={handleSubmit} id="create-presentation-form">
            <div className="form-floating mb-3">
              <input onChange={handleNameChange} value={presenterName} placeholder="Presenter name" required type="text" name="presenter_name" id="presenter_name" className="form-control"/>
              <label htmlFor="presenter_name">Presenter name</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleEmailChange} value={email} placeholder="Presenter email" required type="email" name="presenter_email" id="presenter_email" className="form-control"/>
              <label htmlFor="presenter_email">Presenter email</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleCompanyChange} value={companyName} placeholder="Company name" type="text" name="company_name" id="company_name" className="form-control"/>
              <label htmlFor="company_name">Company name</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleTitleChange} value={titleName} placeholder="Title" required type="text" name="title" id="title" className="form-control"/>
              <label htmlFor="title">Title</label>
            </div>
            <div className="mb-3">
              <label htmlFor="synopsis">Synopsis</label>
              <textarea onChange={handleSynopsisChange} value={synopsis} className="form-control" id="synopsis" rows="3" name="synopsis" className="form-control"/>
            </div>
            <div className="mb-3">
              <select onChange={handleConferenceChange} value={conference} required name="conference" id="conference" className="form-select">
                <option value="">Choose a conference</option>
                {conferences.map((conference) => {
                    return (
                    <option key={conference.href} value={conference.href}>
                        {conference.name}
                    </option>
                    );
                })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  </div>)

}

export default PresentationForm;
