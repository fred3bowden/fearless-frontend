import React from 'react';
import Nav from './Nav';
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import ConferenceForm from './ConferenceForm';
import AttendConferenceForm from './AttendConferenceForm';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import PresentationForm from './PresentationForm';
import MainPage from './MainPage';

function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <div>
      <BrowserRouter>
        <Nav />
        <div className="container">
          <Routes>
            <Route index element={<MainPage/>}/>
            <Route path="presentations/new" element={<PresentationForm/>}/>
            <Route path="conferences/new" element={<ConferenceForm/>} />
            <Route path="locations/new" element={<LocationForm/>} />
            <Route path="attendees" element={<AttendeesList attendees={props.attendees}/>} />
            <Route path="attendees/new" element={<AttendConferenceForm/>} />
          </Routes>
        </div>
      </BrowserRouter>
    </div>

  );
}

export default App;
