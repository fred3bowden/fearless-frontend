window.addEventListener('DOMContentLoaded', async () => {
    const url = "http://localhost:8000/api/states/";

    try {
        const response = await fetch(url);
        if (!response.ok) {
            console.log("No response.");
        }
        else {
            const data = await response.json();
            const states = data.states;

            const selectElement = document.getElementById("state");

            states.forEach(state => {
              const option = document.createElement("option");
              option.value = state.abbreviation;
              option.text = state.name;
              selectElement.appendChild(option);
            });

            const formTag = document.getElementById('create-location-form');
            formTag.addEventListener('submit', async event => {
              event.preventDefault();
              const formData = new FormData(formTag);
              const json = JSON.stringify(Object.fromEntries(formData));

              const locationUrl = 'http://localhost:8000/api/locations/';
              const fetchConfig = {
                method: "post",
                body: json,
                headers: {
                    'Content-Type': 'application/json',
                },
            };
            const response = await fetch(locationUrl, fetchConfig);
            if (response.ok) {
                formTag.reset();
                const newLocation = await response.json();
            }});
        }
    } catch (e) {
        console.log("Error: ", e);
    }
})
