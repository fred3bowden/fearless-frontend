function createCard(name, description, pictureUrl, newStarts, newEnds, locationName) {
    return `
      <div class="col">
        <div style="box-shadow: 7px 7px grey; margin-bottom: 20px;" class="card">
          <img src="${pictureUrl}" alt="card image" class="card-img-top">
          <div class="card-body">
            <h5 class="card-title placeholder-glow">${name}</h5>
            <h6 class="card-subtitle mb2 text-muted placeholder-glow">${locationName}</h6>
            <p class="card-text placeholder-glow">${description}</p>
          </div>
          <div class="card-footer placeholder-glow">
            ${newStarts} - ${newEnds}
          </div>
        </div>
      </div>
    `;
  }

  window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/';
    let columnIndex = 0; // start the column index at zero

    try {
      const response = await fetch(url);
      if (!response.ok) {
        console.log("No response...");
      } else {
        const data = await response.json();
        const conferencePromises = []; //create an empty array that will hold each promise

        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailPromise = fetch(detailUrl) //get the promise details
            .then(response => response.json()) //turn details into json
            .then(details => { //create variables from json data
              const name = details.conference.name;
              const description = details.conference.description;
              const pictureUrl = details.conference.location.picture_url;
              const starts = new Date(details.conference.starts); //new Date is built into js, parse date string into a date object
              const newStarts = starts.toLocaleDateString(); //give it x/xx/xxxx formatting
              const ends = new Date(details.conference.ends);
              const newEnds = ends.toLocaleDateString();
              const locationName = details.conference.location.name;
              const html = createCard(name, description, pictureUrl, newStarts, newEnds, locationName);

              if (columnIndex >= 3) {//0 1 2... 0 1 2
                columnIndex = 0; //reset the index back to 0
              }

              const row = document.querySelector('.row'); //get the row
              const column = row.children[columnIndex]; //make a child div named column for each index
              column.innerHTML += html; //popoulate column div with the created card

              columnIndex++; //increment by 1 for each index
            })
            .catch(error => {
                const errorContainer = document.querySelector('.error-container');
                const errorMessage =
                    `<div class="alert alert-primary" role="alert">
                    An error occurred: ${error}
                    </div>`;
                errorContainer.innerHTML = errorMessage
            }); //must have a catch after trying to fetch each piece of async data and failing

          conferencePromises.push(detailPromise); //each piece of data that was promised is placed in promise array
        }

        await Promise.all(conferencePromises);
        //Promise.all method returns a single promise that resolves when all promises in the array complete
        //Makes sure conference details have been fetched and processed before creating a card
      }
    } catch (error) {
        const errorContainer = document.querySelector('.error-container');
        const errorMessage = `
          <div class="alert alert-primary" role="alert">
            An error occurred: ${error}
          </div>
        `;
        errorContainer.innerHTML = errorMessage;
    }
  });
